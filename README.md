# OpenML dataset: Click

https://www.openml.org/d/45556

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This dataset is a subset of the [KDDCup 2012 track 2](https://www.kaggle.com/competitions/kddcup2012-track2/) data created by Manu Joseph and Harsh Raj for the paper

> Joseph, M., & Raj, H. (2022). 
> GATE: Gated Additive Tree Ensemble for Tabular Classification and Regression. 
> arXiv preprint arXiv:2207.08548v4.

We retrieved the data from [Dropbox](https://www.dropbox.com/s/ry6zsr6qtuz8l5z/click_set_1_.pickle?dl=1).

**Note:** please read the Kaggle dataset description carefully before using this dataset. This dataset mostly contains IDs that should be looked up in other files that are not on OpenML and were not used as part of the benchmark in the paper mentioned above.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45556) of an [OpenML dataset](https://www.openml.org/d/45556). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45556/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45556/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45556/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

